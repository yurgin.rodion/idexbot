from django.urls import path
from rest_framework.routers import SimpleRouter

from .views import TGAccountViewSet, PhoneCodeView, TaskTGView

router = SimpleRouter()
router.register(r'tg_acc', TGAccountViewSet)
urlpatterns = [
                  path(r'phone_code', PhoneCodeView.as_view(), name='phone_code'),
                  path(r'task', TaskTGView.as_view(), name='task')
              ] + router.urls
