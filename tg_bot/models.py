from django.db import models

from main.models import User


class TGBot(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    api_id = models.CharField(max_length=20)
    api_hash = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)
    password = models.CharField(max_length=200, blank=True)

