import redis
from django.conf import settings
from pyrogram import Client
from pyrogram.api.errors import BadRequest

from IdexBot.celery import app
from tg_bot.utils import CustomTask
from .models import TGBot


def phone_code(phone_number):
    r = redis.Redis(
        host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB
    )
    code = None
    while code is None:
        code = r.get('phone_code {}'.format(phone_number))
    r.delete('phone_code {}'.format(phone_number))
    return int(code)


@app.task(base=CustomTask, time_limit=20)
def tg_auth(user_pk, pk, init=None):
    tg_account = TGBot.objects.get(pk=pk)
    if init:
        client = Client(
            'sessions/{}'.format(pk),
            api_id=tg_account.api_id,
            api_hash=tg_account.api_hash,
            phone_number=tg_account.phone_number,
            phone_code=phone_code,
            password=tg_account.password,
            workers=1
        )
        r = redis.Redis(
            host=settings.REDIS_HOST, port=settings.REDIS_PORT,
            db=settings.REDIS_DB
        )
        status = None
        while status != 'Ok':
            try:
                client.start()
            except BadRequest as e:
                status = e.ID
                if e.ID == 'PHONE_NUMBER_BANNED':
                    break
                client = Client(
                    'sessions/{}'.format(pk),
                    api_id=tg_account.api_id,
                    api_hash=tg_account.api_hash,
                    phone_number=tg_account.phone_number,
                    phone_code=phone_code,
                    password=tg_account.password,
                    workers=1
                )
            else:
                status = 'Ok'
                client.stop()
            r.set('phone_code_status {}'.format(tg_account.phone_number),
                  status)
    else:
        client = Client(
            'sessions/{}'.format(pk),
            api_id=tg_account.api_id,
            api_hash=tg_account.api_hash,
            workers=1
        )
        client.start()
        return tg_account, client


@app.task(base=CustomTask)
def tg_join(user_pk, pk, group):
    tg_account, client = tg_auth(user_pk, pk)
    client.join_chat(group)
    client.stop()


@app.task(base=CustomTask)
def tg_message(user_pk, pk, group, message):
    tg_account, client = tg_auth(user_pk, pk)
    client.send_message(group, message)
    client.stop()


@app.task(base=CustomTask)
def tg_leave(user_pk, pk, group):
    tg_account, client = tg_auth(user_pk, pk)
    client.leave_chat(group)
    client.stop()
