from rest_framework import permissions


class IsTGManager(permissions.BasePermission):
    """
    Custom permission to only allow tg manager user.
    """
    def has_permission(self, request, view):
        return request.user.is_tg_manage and request.user.is_authenticated
