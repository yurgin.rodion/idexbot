from random import randint

import redis
from django.conf import settings
from rest_framework import viewsets, views, status
from rest_framework.response import Response

from main.permissions import IsOwner
from .models import TGBot
from .permissions import IsTGManager
from .serializers import TGAccountSerializer
from .tasks import tg_join, tg_leave, tg_message


class TGAccountViewSet(viewsets.ModelViewSet):
    queryset = TGBot.objects.all()
    permission_classes = (IsOwner, IsTGManager)
    serializer_class = TGAccountSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        user = self.request.user
        return self.queryset.filter(owner=user)


class PhoneCodeView(views.APIView):

    def post(self, request, format=None):
        r = redis.StrictRedis(host=settings.REDIS_HOST,
                              port=settings.REDIS_PORT, db=settings.REDIS_DB)
        r.set('phone_code {}'.format(request.data['phone_number']),
              request.data['phone_code'])
        tg_status = None
        while not tg_status:
            tg_status = r.get(
                'phone_code_status {}'.format(request.data['phone_number']))
        r.delete('phone_code_status {}'.format(request.data['phone_number']))
        return Response(data=tg_status,
                        status=status.HTTP_200_OK if tg_status == b'Ok' else status.HTTP_400_BAD_REQUEST)


class TaskTGView(views.APIView):
    permission_classes = (IsTGManager,)

    def post(self, request, format=None):
        actions = {
            'join': tg_join,
            'leave': tg_leave,
            'message': tg_message
        }
        accounts = TGBot.objects.filter(owner=request.user)
        if request.data['min_time']:
            min_time = int(request.data['min_time'])
        else:
            min_time = None
        if request.data['max_time']:
            max_time = int(request.data['max_time'])
        else:
            max_time = None
        if min_time and max_time:
            countdown = randint(min_time,
                                max_time)
        elif min_time or max_time:
            countdown = min_time or max_time
        else:
            countdown = 0
        for account in accounts:
            if request.data['action'] == 'message':
                actions[request.data['action']].apply_async(
                    (request.user.pk, account.pk,
                     request.data['group'],
                     request.data['message']),
                    countdown=countdown)
            else:
                actions[request.data['action']].apply_async(
                    (request.user.pk, account.pk,
                     request.data['group']),
                    coundown=countdown)
            if min_time and max_time:
                countdown += randint(min_time,
                                     max_time)
            elif min_time or max_time:
                countdown += min_time or max_time
        return Response('Ok')
