from celery import Task
from user_messages import api as messages

from main.models import User


class CustomTask(Task):
    abstract = True

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        if 'ID' in dir(exc):
            name = exc.ID
        else:
            name = type(exc).__name__
        if 'x' in dir(exc) and 'MESSAGE' in dir(exc):
            message = exc.MESSAGE.format(x=exc.x)
        elif 'MESSAGE' in dir(exc):
            message = exc.MESSAGE
        else:
            message = 'some error just happened'
        messages.error(User.objects.get(pk=args[0]),
                       '{}: {}'.format(name, message),
                       deliver_once=False)
