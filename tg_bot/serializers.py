from rest_framework import serializers

from .models import TGBot


class TGAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = TGBot
        fields = ('api_id', 'api_hash', 'phone_number', 'password', 'id')
        read_only_fields = ('id',)
