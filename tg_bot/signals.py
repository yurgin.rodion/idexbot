from django.db.models.signals import post_save
from django.dispatch import receiver

from IdexBot.celery import app
from .models import TGBot
from .tasks import tg_auth


@app.on_after_configure.connect
@receiver(post_save, sender=TGBot)
def bot_post_save(sender, instance, **kwargs):
    tg_auth.apply_async((instance.owner.pk, instance.pk, True), expires=120)
