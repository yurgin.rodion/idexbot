from django.apps import AppConfig


class TgBotConfig(AppConfig):
    name = 'tg_bot'

    def ready(self):
        import tg_bot.signals
