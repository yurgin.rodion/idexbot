function deleteBot(id) {
    var $url = 'api/bot/' + id;
    var $inner = $('stop' + id);
    $.ajax({
        method: 'delete',
        url: $url,
        async: false,
        dataType: 'json',
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        success: function (data) {
            reloadBots();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $inner.prepend("<p class='bad'>" + thrownError + xhr.statusText + xhr.responseText + "</p>");
        }
    });
}


function reloadBots() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "api/bot", false);
    xhr.send();
    if (xhr.status != 200) {
        alert(xhr.status + ': ' + xhr.statusText);
    }
    else {
        $('.bot').remove();
        var JSONResponse = JSON.parse(xhr.responseText);
        for (var i = 0; i < JSONResponse.length; i++) {
            var id = JSONResponse[i]["id"];
            var market = JSONResponse[i]["market"];
            var date = new Date(JSONResponse[i]["date"]).toLocaleString();
            $('#bots').prepend(
                '<tr class="bot">' +
                '<th abbr="id">' + id + '</th>' +
                '<th abbr="market">' + market + '</th>' +
                '<th abbr="sum1">' + JSONResponse[i]["sum1_field"] + '</th>' +
                '<th abbr="date">' + date + '</th>' +
                '<th abbr="stop-btn" id="stop' + id + '">' +
                '<button class="btn btn-default" onclick="deleteBot(' + id + ')">Stop</button></th>' +
                '</tr>'
            )

        }
    }
}


function addMessages(messages) {
    var num = messages.length;
    $('.num').html('<span id="num">' + num + '</span>');
    var $messages = $('#messages');
    $messages.text('');
    for (var i = 0; i < messages.length; i++) {
        var time = new Date(messages[i]['time']).toLocaleString();
        $messages.prepend('' +
            '<li>' +
            '<span class="icon">' +
            '<i class="fas fa-exclamation-triangle"></i></span>' +
            '<span class="text">' + messages[i]['message'] + '</span>' +
            '<span class="time">' + time + '</span>' +
            '</li>')
    }
}

function messagesPolling() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "api/messages", false);
    xhr.send();
    var messages = JSON.parse(xhr.responseText)['messages'];
    if(messages.length){
        addMessages(messages)
    }
}

reloadBots();
setInterval(messagesPolling, 10000);


$(document).ready(function () {
    var $form = $("#form-bot");
    var $inner = $("#form-bot .response");

    var $form_log = $("#logout");
    var $inner_log = $("#logout .response");

    var $form_crypto = $("#form-crypto");
    var $inner_crypto = $("#form-crypto .response");

    $form.on('submit', function (e) {
        sendForm($form, $inner, e, true)
    });
    $form_log.on('submit', function (e) {
        sendForm($form_log, $inner_log, e)
    });
    $form_crypto.on('submit', function (e) {
        sendForm($form_crypto, $inner_crypto, e)
    });
});