function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function sendForm($form, $inner, e, reload) {

    var $url = $form.attr('action');
    var $method = $form.attr('method');
    e.preventDefault();
    var $data = $form.serialize();
    $.ajax({
        method: $method,
        url: $url,
        data: $data,
        async: false,
        dataType: 'json',
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        success: function (data) {
            if (reload)
                reloadBots();
            else
                location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $inner.html("<p class='bad'>" + xhr.responseText + "</p>");
        }
    });
}


$(document).ready(function () {
    var $form_login = $("#form-login");
    var $inner_login = $("#form-login .response");

    $form_login.on('submit', function (e) {
        sendForm($form_login, $inner_login, e)
    });
});