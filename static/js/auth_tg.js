function sendForm($form, $inner, e, reload, method) {

    var $url = $form.attr('action');
    var $method = $form.attr('method');
    e.preventDefault();
    var $data = $form.serialize();
    $.ajax({
        method: $method,
        url: $url,
        data: $data,
        async: true,
        dataType: 'json',
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        success: function (data) {
            if (reload) {
                reloadAccounts();
                method(data);
            }
            else
                location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $inner.html("<p class='bad'>" + xhr.responseText + "</p>");
        }
    });
}

function deleteAccount(id) {
    var $url = '/tg_bot/tg_acc/' + id;
    var $inner = $('stop' + id);
    $.ajax({
        method: 'delete',
        url: $url,
        async: false,
        dataType: 'json',
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        },
        success: function (data) {
            reloadAccounts();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $inner.prepend("<p class='bad'>" + thrownError + xhr.statusText + xhr.responseText + "</p>");
        }
    });
}


function reloadAccounts() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "/tg_bot/tg_acc", false);
    xhr.send();
    if (xhr.status != 200) {
        alert(xhr.status + ': ' + xhr.statusText);
    }
    else {
        $('.account').remove();
        var JSONResponse = JSON.parse(xhr.responseText);
        for (var i = 0; i < JSONResponse.length; i++) {
            var id = JSONResponse[i]["id"];
            var phone_number = JSONResponse[i]["phone_number"];
            $('#accounts').prepend(
                '<tr class="account">' +
                '<th abbr="id">' + id + '</th>' +
                '<th abbr="phone_number">' + phone_number + '</th>' +
                '<th abbr="delete-btn" id="delete' + id + '">' +
                '<button class="btn btn-default" onclick="deleteAccount(' + id + ')">Stop</button></th>' +
                '</tr>'
            )

        }
    }
}


function addMessages(messages) {
    var num = messages.length;
    $('.num').html('<span id="num">' + num + '</span>');
    var $messages = $('#messages');
    $messages.text('');
    for (var i = 0; i < messages.length; i++) {
        var time = new Date(messages[i]['time']).toLocaleString();
        $messages.prepend('' +
            '<li>' +
            '<span class="icon">' +
            '<i class="fas fa-exclamation-triangle"></i></span>' +
            '<span class="text">' + messages[i]['message'] + '</span>' +
            '<span class="time">' + time + '</span>' +
            '</li>')
    }
}

function messagesPolling() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "api/messages", false);
    xhr.send();
    var messages = JSON.parse(xhr.responseText)['messages'];
    if(messages.length){
        addMessages(messages)
    }
}

setInterval(messagesPolling, 10000);
reloadAccounts();

$(document).ready(function () {
    var $form = $("#tg-account-form");
    var $inner = $("#tg-account-form .response");

    var $form_phone_code = $("#phone_code_form");
    var $inner_phone_code = $("#phone_code_form .response");

    var $form_action = $("#form-action");
    var $inner_action = $("#form-action .response");

    $form.on('submit', function (e) {
        sendForm($form, $inner, e, true, function (data) {
            $('#phone_number_code').delete;
            $form_phone_code.prepend('<input type="text" id="phone_number_code" name="phone_number" value="'+data['phone_number']+'" hidden>')
        });
        $('#phone_code_form').collapse('show');
    });
    $form_phone_code.on('submit', function (e) {
        sendForm($form_phone_code, $inner_phone_code, e, false, function (data) {
            $('#phone_number_code').delete;
            $('#phone_code_form').collapse('hide');
        });
    });
    $form_action.on('submit', function (e) {
        sendForm($form_action, $inner_action, e)
    });
});