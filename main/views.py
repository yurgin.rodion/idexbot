from rest_framework import viewsets, views, permissions
from rest_framework.response import Response
from user_messages import api as messages

from .models import Bot, User
from .permissions import IsOwner, IsCryptoManage
from .serializers import BotSerializer, UserSerializer


class BotView(viewsets.ModelViewSet):
    queryset = Bot.objects.all()
    serializer_class = BotSerializer
    permission_classes = (IsOwner, IsCryptoManage,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        user = self.request.user
        return self.queryset.filter(owner=user)


class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class MessagesView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = User.objects.get(pk=request.user.id)
        msgs = messages.get_messages(user=user)
        msgs = sorted(msgs, key=lambda x: x.created_at)[-10:]
        result = []
        for msg in msgs:
            result.append({'message': msg.message, 'status': msg.level_tag,
                           'time': msg.created_at})
        return Response({'messages': result})
