from django.urls import path
from rest_framework.routers import SimpleRouter

from .views import BotView, UserView, MessagesView

router = SimpleRouter()
router.register(r'bot', BotView)
router.register(r'user', UserView)
urlpatterns = [
                  path(r'messages', MessagesView.as_view(), name='messages')
              ] + router.urls
