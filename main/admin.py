from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

# Register your models here.
from .models import Bot, User

admin.site.register(Bot)


@admin.register(User)
class UserAdminSite(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'),
         {'fields': ('first_name', 'last_name', 'email', 'exchange')}),
        (_('Permissions'), {'fields': (
                        'is_active', 'is_staff', 'is_superuser', 'is_tg_manage',
                        'is_crypto_manage',
                        'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
