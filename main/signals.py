from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django_celery_beat.models import PeriodicTask
from django.db.models import ObjectDoesNotExist

from IdexBot.celery import app
from .models import Bot, Order
from .tasks import start_bot, stop_bot


@app.on_after_configure.connect
@receiver(post_save, sender=Bot)
def bot_post_save(sender, instance, **kwargs):
    start_bot.delay(instance.pk, instance.owner.pk)


@app.on_after_configure.connect
@receiver(pre_delete, sender=Bot)
def bot_pre_delete(sender, instance, **kwargs):
    try:
        order = Order.objects.get(bot=instance).order_id
        owner = instance.owner.pk
        stop_bot.delay(instance.pk, owner, order, instance.market)
        PeriodicTask.objects.get(
            name='polling bot {}'.format(instance.pk),
        ).delete()
    except ObjectDoesNotExist as e:
        pass
