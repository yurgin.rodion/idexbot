from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator
from django.db import models
from django.utils import timezone


class User(AbstractUser):
    api_key = models.CharField(max_length=100)
    secret = models.CharField(max_length=100)
    exchange = models.CharField(max_length=20, default='binance')
    is_tg_manage = models.BooleanField(default=False)
    is_crypto_manage = models.BooleanField(default=False)


class Bot(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    sum_field = models.FloatField('Начальная сумма закупа/продажи(Sum)')
    adv = models.FloatField('Adv', validators=[MaxValueValidator(100)])
    side_field = models.CharField(max_length=4)
    type_field = models.CharField(max_length=6)
    is_active = models.BooleanField(default=False)
    date = models.DateTimeField('Время создания бота', default=timezone.now)
    market = models.CharField(max_length=12)
    limit_price = models.FloatField('Макс(закуп) или мин(продажа) цена')
    rival = models.PositiveSmallIntegerField('Rival',
                                             validators=[
                                                 MaxValueValidator(100)])

    sum1_field = models.FloatField('Оставшаяся сумма закупа/продажи(Sum1)',
                                   null=True)
    start_price = models.FloatField('Начальная цена', null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.sum1_field = self.sum_field
        super(Bot, self).save(force_insert, force_update, using, update_fields)


class Order(models.Model):
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE)
    order_id = models.CharField(max_length=200, blank=True)
