from rest_framework import serializers

from .models import Bot, User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('api_key', 'secret', 'id')
        read_only_fields = ('id',)


class BotSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Bot
        fields = ('id', 'sum_field', 'sum1_field', 'limit_price',
                  'rival', 'adv', 'start_price', 'is_active', 'date', 'owner',
                  'market', 'type_field', 'side_field')
        read_only_fields = ('id', 'sum1_field', 'is_active', 'date')
