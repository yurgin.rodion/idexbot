from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow Author of an object.
    """
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user


class IsCryptoManage(permissions.BasePermission):
    """
    Custom permission to only allow Author of an object.
    """
    def has_permission(self, request, view):
        return request.user.is_crypto_manage and request.user.is_authenticated
