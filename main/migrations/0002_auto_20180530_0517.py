# Generated by Django 2.0.4 on 2018-05-30 05:17

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='adv',
            field=models.FloatField(validators=[django.core.validators.MaxValueValidator(100)], verbose_name='Adv'),
        ),
        migrations.AlterField(
            model_name='bot',
            name='limit_price',
            field=models.FloatField(verbose_name='Макс(закуп) или мин(продажа) цена'),
        ),
        migrations.AlterField(
            model_name='bot',
            name='start_price',
            field=models.FloatField(null=True, verbose_name='Начальная цена'),
        ),
        migrations.AlterField(
            model_name='bot',
            name='sum1_field',
            field=models.FloatField(null=True, verbose_name='Оставшаяся сумма закупа/продажи(Sum1)'),
        ),
        migrations.AlterField(
            model_name='bot',
            name='sum_field',
            field=models.FloatField(verbose_name='Начальная сумма закупа/продажи(Sum)'),
        ),
    ]
