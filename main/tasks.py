import json
import logging
import time
from decimal import Decimal

import ccxt
from ccxt import NetworkError
from django.conf import settings
from django_celery_beat.models import IntervalSchedule, PeriodicTask
from user_messages import api as messages

from IdexBot.celery import app
from .models import Bot, Order, User
from .utils import CustomTask, InvalidTotalError


def get_client(owner, exchange):
    client = getattr(ccxt, exchange)()
    client.apiKey = owner.api_key
    client.secret = owner.secret
    return client


def get_order_book(client, bot):
    order_book = client.fetch_order_book(bot.market)
    return order_book['asks'] if bot.side_field == 'sell' \
        else order_book['bids']


def get_max_price_total(client, bot, current_price):
    """
    Возвращает максимальную цену с total больше или равно sum1 * rival%
    :param current_price: цена настоящего ордера
    :param client:
    :param bot:
    :return:
    """
    order_book = get_order_book(client, bot)
    if bot.side_field == 'sell':
        reverse = False
    else:
        reverse = True
    order_book.sort(key=lambda x: x[0], reverse=reverse)
    for order in order_book:
        if bot.side_field == 'sell':
            total = order[1]
        else:
            total = order[0] * order[1]
        if total >= bot.sum1_field * bot.rival / 100:
            return order[0]
        if order[0] < current_price:
            return


def get_max_price(client, bot):
    order_book = get_order_book(client, bot)
    if bot.side_field == 'sell':
        return min(order_book, key=lambda x: x[0])[0]
    return max(order_book, key=lambda x: x[0])[0]


def create_order(client, bot, price=None):
    """
    Создает ордер и вычесляет цену
    :param price:
    :param client:  Объект для работы с API
    :param bot:     Объект с необходимыми данными для бота
    :return: созданный ордер
    """
    if bot.start_price and not price:
        start_price = bot.start_price
    else:
        start_price = price * bot.adv / 100
        if bot.side_field == 'sell':
            start_price = price - start_price
        else:
            start_price = price + start_price
        start_price = round(start_price, 6)
    if bot.sum1_field < 0.01:
        raise InvalidTotalError('Total less then 0.01')

    if start_price > bot.limit_price and bot.side_field == 'buy' \
            or start_price < bot.limit_price and bot.side_field == 'sell':
        start_price = bot.limit_price

    if bot.side_field == 'sell':
        amount = bot.sum1_field
    else:
        amount = float(Decimal(str(bot.sum1_field / start_price)).quantize(
            Decimal('1.00')))
    while amount * start_price < 0.01:
        amount += 0.01

    amount = float(Decimal(str(amount)).quantize(
        Decimal('1.00')))

    logging.error(
        "create_order market:{} amount:{} price:{} bot:{}".format(bot.market,
                                                                  amount,
                                                                  start_price,
                                                                  bot.id))
    my_order = client.create_order(bot.market, bot.type_field,
                                   bot.side_field,
                                   amount, start_price)
    return my_order


def cancel_order(client, order_id, market, user):
    """
    Отменяет ордер
    :param client:  Объект для работы с API
    :param order_id:
    :param market: название маркета на котором ордер
    :param user: пользователь которому будет отправлено сообщение об ошибке
    :return:
    """
    try:
        client.cancel_order(order_id, market)
    except NetworkError:
        time.sleep(2)
        cancel_order(client, order_id, market, user)


def cancel_orders(client, bot):
    my_orders = client.fetch_open_orders(bot.market)

    for my_order in my_orders:
        cancel_order(client, my_order['id'], bot.market, bot.owner)


@app.task(base=CustomTask, ignore_result=True)
def start_bot(pk, user_pk):
    """
    При запуске отменяет все ордера на аккаунте биржи,
    выставляет ордер и сохраняет его хэш в БД
    :param pk: первичный ключ в бд для бота
    :param user_pk: первичный ключ пользователя запустившего бота
    :return:
    """
    bot = Bot.objects.get(pk=pk)
    client = get_client(bot.owner, bot.owner.exchange)
    logging.error(
        "id:{} sum:{} sprice:{} lprice:{} rival:{} adv:{}".format(bot.id,
                                                                  bot.sum1_field,
                                                                  bot.start_price,
                                                                  bot.limit_price,
                                                                  bot.rival,
                                                                  bot.adv))

    cancel_orders(client, bot)

    if not bot.start_price:
        price = get_max_price(client, bot)
    else:
        price = None

    my_order = create_order(client, bot, price)

    Order.objects.create(order_id=my_order['id'], bot=bot)
    schedule, created = IntervalSchedule.objects.get_or_create(
        every=settings.POLLING_TIME_SEC,
        period=IntervalSchedule.SECONDS
    )
    PeriodicTask.objects.create(
        interval=schedule,
        name='polling bot {}'.format(pk),
        task='main.tasks.polling',
        args=json.dumps([pk, user_pk])
    )


@app.task(base=CustomTask, ignore_result=True)
def polling(pk, user_pk):
    """
    Проверяет наличие ордеров с обьемом закупа SUM1 * RIVAL,
    если есть такие, то берется максимальная цена
    и выставляется ордер с ценой MAX+ADV%
    :param pk: Первичный ключ в бд для бота
    :param user_pk: первичный ключ пользователя запустившего бота
    :return:
    """
    bot = Bot.objects.get(pk=pk)
    order_ = Order.objects.get(bot=bot)

    client = get_client(bot.owner, bot.owner.exchange)

    order = client.fetch_order(order_.order_id, bot.market)

    if bot.side_field == 'sell':
        sum1 = order['remaining']
    else:
        sum1 = order['remaining'] * order['price']

    Bot.objects.filter(pk=pk).update(sum1_field=sum1)

    if sum1 == 0 or order['status'] != 'open':
        Order.objects.get(bot=bot).delete()
        if sum1 == 0:
            messages.info(bot.owner,
                          'Bot order {} is completely filled or '.format(
                              bot.id),
                          deliver_once=False)
        else:
            messages.info(bot.owner,
                          'The tokens ran out (bot {})'.format(bot.id),
                          deliver_once=False)
        PeriodicTask.objects.get(
            name='polling bot {}'.format(bot.pk),
        ).delete()
        max_price = 0
    else:
        max_price = get_max_price_total(client, bot, order['price'])
        if max_price == order['price'] or order['price'] == bot.limit_price:
            max_price = 0

    if max_price:
        cancel_order(client, order_.order_id, bot.market, bot.owner)
        order = client.fetch_order(order_.order_id, bot.market)
        if bot.side_field == 'sell':
            sum1 = order['remaining']
        else:
            sum1 = order['remaining'] * order['price']
        Bot.objects.filter(pk=pk).update(sum1_field=sum1)

        bot = Bot.objects.get(pk=pk)
        my_order = create_order(client, bot, max_price)
        Order.objects.filter(bot=bot).update(order_id=my_order['id'])


@app.task(base=CustomTask, ignore_result=True)
def stop_bot(pk, user_pk, order_id, market):
    """
    Отменяет ордер который обслуживался ботом
    :param pk: Первичный ключ в бд для бота
    :param order_id: id ордера
    :param user_pk: первичный ключ пользователя запустившего бота
    :param market: название маркета на котором ордер
    :return:
    """
    owner = User.objects.get(pk=user_pk)
    client = get_client(owner, owner.exchange)
    cancel_order(client, order_id, market, owner)
