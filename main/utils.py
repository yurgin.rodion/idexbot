from ccxt import InsufficientFunds, ExchangeError, OrderNotFound
from celery import Task
from user_messages import api as messages

from .models import User


class CustomTask(Task):
    abstract = True

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        errors = {
            InsufficientFunds: 'Account has insufficient balance for requested action.',
            ExchangeError: 'some error just happened',
            OrderNotFound: 'Order not found by bot {}'.format(args[0]),
            InvalidTotalError: 'Total(sum) must be at least 0.01',
        }
        if type(exc) in errors:
            messages.error(User.objects.get(pk=args[1]), errors[type(exc)],
                           deliver_once=False)
        else: pass


class InvalidTotalError(ExchangeError):
    """Raised when you try to set total less than 0.01"""
    pass
