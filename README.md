# CryptoBot
**Before start:**

Edit `localhost` in crypto_nginx.conf

Install [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html)

Create and activate virtualenv:
```bash
mkvirtualenv --python=python3.5 crypto_bot_env
```
Install requirements:
```bash
pip install -r requirements.txt
```

Collect static
```bash
python mange.py collectstatic
```

Replace in `celery.service`, `celerybeat.service` and `uwsgi.service` 
`%venv%` on `/path/to/virtualenv/crypto_bot_env/bin`, 
`%proj%` on `/path/to/IdexBot`

Edit Environment variables in `uwsgi.service`

Add and enable services:
```bash
sudo cp celery.service /lib/systemd/system/
sudo cp celerybeat.service /lib/systemd/system/
sudo cp uwsgi.service /lib/systemd/system/
sudo systemctl enable celery.service
sudo systemctl enable celerybeat.service
sudo systemctl enable uwsgi.service
```

Add link nginx conf:
```bash
sudo ln -s /path/to/IdexBot/crypto_nginx.conf /etc/nginx/sites-enabled/
```

Restart Nginx

**Start server:**

````bash
redis-server --daemonize yes
sudo systemctl start celery.service
sudo systemctl start celerybeat.service
sudo systemctl start uwsgi.service
````
